package tdd.tests;

import org.junit.jupiter.api.Test;
import tdd.Calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CalculatorTest {
    @Test
    void addEmptyStringReturnsZero() {
        assertEquals(0, Calculator.add(""));
    }
}