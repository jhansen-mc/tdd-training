##### TDD Training
* Choose a language (Java or Angular)
* Do this one line/bullet at a time!
* Don’t read ahead or solve problems on later lines!
* This will help you see how you’d tackle a problem with TDD.
* Don't forget to `npm install` if you choose Angular!



Write Calculator class with one method: **add**

`public static int add(final String numbers) {
}`

1. The method can take 0, 1 or 2 numbers, and will return their sum (for an empty string it will return 0) for example “” or “1” or “1,2”
   * Write test case for “”, check it fails.
   * Then make it pass in the most simple way and refactor.
   * Write test case for “1”, check it fails.
   * Then make it pass in the most simple way and refactor.
   * Write test case for “1,2”, check it fails.
Then make it pass in the most simple way and refactor.
2. Allow the Add method to handle an unknown amount of numbers
3. Allow the Add method to handle new lines between numbers (in addition to commas).
the following input is ok:  “1\n2,3”  (will equal 6) 
the following input is NOT ok:  “1,\n” (don’t need to prove it - just clarifying)
4. Keep doing the example at https://osherove.com/tdd-kata-1/ 

